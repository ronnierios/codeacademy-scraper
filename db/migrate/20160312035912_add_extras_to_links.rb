class AddExtrasToLinks < ActiveRecord::Migration
  def change
    add_column :links, :extras, :integer
  end
end
