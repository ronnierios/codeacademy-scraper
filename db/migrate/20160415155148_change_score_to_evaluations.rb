class ChangeScoreToEvaluations < ActiveRecord::Migration
  def up
    change_column :evaluations, :score, :float
  end
  def down
    change_column :evaluations, :score, :integer
  end
end
