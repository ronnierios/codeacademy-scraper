class ChangeScoreToLinks < ActiveRecord::Migration
  def up
    change_column :links, :score, :float
  end
  def down
    change_column :links, :score, :integer
  end
end
