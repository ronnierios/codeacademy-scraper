class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :course_id
      t.integer :link_id
      t.date :completed_at
      t.boolean :submited

      t.timestamps null: false
    end
  end
end
