class AddRateToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :rate, :integer
  end
end
