# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
    
    Course.create(name: "HTML & CSS", deadline: "Mar 12, 2016", rate: 2)
    Course.create(name: "Javascript", deadline: "Mar 12, 2016", rate: 2)
    Course.create(name: "jQuery", deadline: "Mar 12, 2016", rate: 2)
    Course.create(name: "PHP", deadline: "Mar 12, 2016", rate: 2)
    Course.create(name: "Make an Interactive Website", deadline: "Mar 12, 2016", rate: 2)
    Course.create(name: "Ruby", deadline: "Apr 8, 2016", rate: 1)