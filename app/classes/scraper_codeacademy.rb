require 'open-uri'
require 'mechanize'

class ScraperCodeacademy
    
    attr_accessor :url
    
    def initialize
        @agent = Mechanize.new 
        @agent.get('https://codecademy.com/')
        @agent.page.link_with(href: /login/).click
        @agent.page.forms.first['user[login]'] = ENV["CODECADEMY_USER"]
        @agent.page.forms.first['user[password]'] = ENV["CODECADEMY_PASS"]
        @agent.page.forms.first.submit
        puts puts "Autenticado"
    end
    
    def analyse_account(url)
        url_achievement = validate_achievements_url(url)
        url_achievement = url_achievement[url_achievement.index('com')+3..-1]
        @agent.get url_achievement,[],@agent.page.link_with(:text => "My Path").referer
    end
    
    def parsed_document
        @document ||= Nokogiri::HTML(open(@url))
    end
    
    def name
        page.search('.avatar ~ a').text
    end
    
    def page
        #@page || @agent.page
        @agent.page
    end
    
    ['JavaScript', 'PHP', 'jQuery','Ruby', 'HTML'].each do |item|
        define_method("#{item.downcase}_completed?") do
            page.body.include?("Complete all units in #{item}")
        end
        define_method("#{item.downcase}_completed_at") do
            page.at("h5:contains('Complete all units in #{item}')").parent.at("small small").text if page.body.include?("Complete all units in #{item}")
        end
    end
    
    def last_completed?
        ['Flipboard','Status Update','News Reader','Push Menu'].each do |item|
            return false unless page.body.include?("Interactive Website: #{item}") 
        end
        return true
    end
    
    def last_completed_at
        page.at("h5:contains('Interactive Website')").parent.at("small small").text if last_completed?
    end
    
    def score
        score = 0
        score += 2 if php_completed?
        score += 2 if javascript_completed?
        score += 2 if jquery_completed?
        score += 2 if html_completed?
        score += 2 if last_completed?
        
        puts "SCOREEEE #{score}"
        score
    end
    
    private 
    def validate_achievements_url(url)
        unless url.index("/achievements")
            if url.index("es/")
                p = url.index("es/")
                url.insert(p+3,"users/").insert(url.length,'/achievements')
            else
                p = url.index('com/')
                url.insert(p+4,"users/").insert(url.length,'/achievements')
            end
        end
        return url.to_s
    end
    
end
