require 'open-uri'
require 'mechanize'

class ScraperEva

    def self.get_students_links
        mem = Mechanize.new
        mem.get('http://virtual.uca.edu.ni/')
        mem.page.link_with(text: 'Log in').click
        mem.page.forms.first['username'] = ENV["EVA_USER"]
        mem.page.forms.first['password'] = ENV["EVA_PASS"]
        mem.page.forms.first.submit
        mem.page.link_with(:href => /course\/view.php\?id=5119/).click
        mem.page.link_with(:href => /assign\/view.php\?id=264190/).click
        mem.page.link_with(:href => /assign\/view.php\?id=264190&action=grading/).click
        mem.page.body.scan(/https:\/\/www.codecademy.com(?:\/es)?\/[A-z0-9]+/).uniq
    end

end