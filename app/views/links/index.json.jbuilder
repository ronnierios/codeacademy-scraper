json.array!(@links) do |link|
  json.extract! link, :id, :name, :url, :comment, :score
  json.url link_url(link, format: :json)
end
