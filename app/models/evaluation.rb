# == Schema Information
#
# Table name: evaluations
#
#  id           :integer          not null, primary key
#  course_id    :integer
#  link_id      :integer
#  completed_at :date
#  submited     :boolean
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Evaluation < ActiveRecord::Base
    belongs_to :course
    belongs_to :link
    
    validates_presence_of :course_id, :link_id, :submited
    
    before_save :process_score
    
    scope :html, -> { joins(:course).where("courses.name = ?", 'HTML & CSS') }
    scope :js, -> { joins(:course).where("courses.name = ?", 'JavaScript')  }
    scope :jquery, -> { joins(:course).where("courses.name = ?", 'jQuery') }
    scope :php, -> { joins(:course).where("courses.name = ?", 'PHP')  }
    scope :ruby, -> { joins(:course).where("courses.name = ?", 'Ruby')  }
    scope :makewebsite, -> { joins(:course).where("courses.name = ?", 'Make an Interactive Website')  }
    
    def exceeded_deadline?
        return self.completed_at > self.course.deadline
    end
    
    private 
    
    def process_score
        self.score = exceeded_deadline? ? self.course.rate / 2.0 : self.course.rate
    end
end
