# == Schema Information
#
# Table name: courses
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Course < ActiveRecord::Base
    validates_presence_of :name
    
    has_many :evaluations
    has_many :links, through: :evaluations
    
    scope :html, -> { where(name: 'HTML & CSS') }
    scope :js, -> { where(name: 'JavaScript') }
    scope :jquery, -> { where(name: 'jQuery') }
    scope :php, -> { where(name: 'PHP') }
    scope :ruby, -> { where(name: 'Ruby') }
    scope :makewebsite, -> { where(name: 'Make an Interactive Website') }
end
