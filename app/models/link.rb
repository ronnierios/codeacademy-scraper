# == Schema Information
#
# Table name: links
#
#  id         :integer          not null, primary key
#  name       :string
#  url        :string
#  comment    :text
#  score      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  extras     :integer
#  status     :string
#

class Link < ActiveRecord::Base
    
    validates_presence_of :name, :url
    
    has_many :evaluations, dependent: :delete_all
    has_many :courses,  through: :evaluations
    
    #after_create :process_new_link
    #before_save :validates_url
    
    private
    
    def validates_url
        # parts = self.url.split('/')
        # if parts.count == 5 and parts.last != 'achievements'
        #     self.url = self.url + '/achievements' if self.url[-1] != '/'
        #     self.url = self.url + 'achievements' if self.url[-1] == '/'
        # end
    end
    
    def process_new_link
       scraper = ScraperCodeacademy.new(self.url)
       
       self.score = scraper.score
       puts "The score isss: #{self.score}"
       self.save!
    end
    
    
end
