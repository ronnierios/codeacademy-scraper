# == Schema Information
#
# Table name: links
#
#  id         :integer          not null, primary key
#  name       :string
#  url        :string
#  comment    :text
#  score      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  extras     :integer
#  status     :string
#

class LinksController < ApplicationController
  before_action :set_link, only: [:show, :edit, :update, :destroy]

  # GET /links
  # GET /links.json
  def index
    #generate
    @links = Link.includes(:courses).all
  end

  # GET /links/1
  # GET /links/1.json
  def show
  end
  
  def review
    Link.destroy_all
    generate
    #sleep 2
    @links = Link.includes(:courses).all
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  # GET /links/new
  def new
    @link = Link.new
  end

  # GET /links/1/edit
  def edit
  end

  # POST /links
  # POST /links.json
  def create
    @link = Link.new(link_params)

    respond_to do |format|
      if @link.save
        format.html { redirect_to @link, notice: 'Link was successfully created.' }
        format.json { render :show, status: :created, location: @link }
      else
        format.html { render :new }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /links/1
  # PATCH/PUT /links/1.json
  def update
    respond_to do |format|
      if @link.update(link_params)
        format.html { redirect_to @link, notice: 'Link was successfully updated.' }
        format.json { render :show, status: :ok, location: @link }
      else
        format.html { render :edit }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /links/1
  # DELETE /links/1.json
  def destroy
    @link.destroy
    respond_to do |format|
      format.html { redirect_to links_url, notice: 'Link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def link_params
      params.require(:link).permit(:name, :url, :comment)
    end
    
    # l.evaluations.joins(:courses)
    # l.evaluations.joins(:courses).where(courses: { name: 'HTML & CSS' }).references(:courses)
    
    def generate
      scraper = ScraperCodeacademy.new
      html = Course.find_by(name: 'HTML & CSS')
      js = Course.find_by(name: 'JavaScript')
      jquery = Course.find_by(name: 'jQuery')
      php = Course.find_by(name: 'PHP')
      ruby = Course.find_by(name: 'Ruby')
      makeWebsite = Course.find_by(name: 'Make an Interactive Website')
      #get_links_test.each do |uri|
      ScraperEva.get_students_links.each do |url|
        #sleep 1
        begin
          scraper.analyse_account(url)
          link = Link.new(name: scraper.name, score: scraper.score, url: url)
          link.save!
        rescue Exception => ex
          flash[:error] = "#{ex}"
          Link.new(name: 'Desconocido',score: 0, url: url, comment: 'Error al analizar el enlace').save!
        end
        begin
          html.evaluations.create(submited: true, completed_at: scraper.html_completed_at, :link => link) if scraper.html_completed?
          js.evaluations.create(submited: true, completed_at: scraper.javascript_completed_at, :link => link) if scraper.javascript_completed?
          jquery.evaluations.create(submited: true, completed_at: scraper.jquery_completed_at, :link => link) if scraper.jquery_completed?
          php.evaluations.create(submited: true, completed_at: scraper.php_completed_at, :link => link) if scraper.php_completed?
          ruby.evaluations.create(submited: true, completed_at: scraper.ruby_completed_at, :link => link) if scraper.ruby_completed?
          makeWebsite.evaluations.create(submited: true, completed_at: scraper.last_completed_at, :link => link) if scraper.last_completed?
        rescue Exception => ex
          flash[:error] = "#{ex.error}"
        end
      end
    end
    
    def get_links_test
      ["https://www.codecademy.com/carloslrubi", "https://www.codecademy.com/es/pyJumper01856","https://www.codecademy.com/es/Francesc301"]
      #["https://www.codecademy.com/carloslrubi", "https://www.codecademy.com/es/pyJumper01856", "https://www.codecademy.com/es/alex10l", "https://www.codecademy.com/CarlosRamirez97", "https://www.codecademy.com/AlejandroMeneses02", "https://www.codecademy.com/Fultie", "https://www.codecademy.com/Alondraptr", "https://www.codecademy.com/bitSolver58281", "https://www.codecademy.com/diomel", "https://www.codecademy.com/es/LetyTellez", "https://www.codecademy.com/Cisko97", "https://www.codecademy.com/es/deathstroke14", "https://www.codecademy.com/coreMaster32397", "https://www.codecademy.com/es/MeylingMendozaMejia", "https://www.codecademy.com/es/teraSlayer04129", "https://www.codecademy.com/es/noheliagm", "https://www.codecademy.com/Malvov", "https://www.codecademy.com/Izabard", "https://www.codecademy.com/es/RocioLG", "https://www.codecademy.com/es/systemJumper65177", "https://www.codecademy.com/es/dprado", "https://www.codecademy.com/es/dprado10", "https://www.codecademy.com/ximenasilvaquintero", "https://www.codecademy.com/sekainoshihaisha", "https://www.codecademy.com/es/YosselinContreras", "https://www.codecademy.com/es/arrayAce49898", "https://www.codecademy.com/es/JasonCoreaBravo", "https://www.codecademy.com/es/boardAce72051", "https://www.codecademy.com/BleeperCat", "https://www.codecademy.com/es/Zaiyan97x", "https://www.codecademy.com/es/Francesc301", "https://www.codecademy.com/jaelgoco", "https://www.codecademy.com/users", "https://www.codecademy.com/es/JoshuaLeviAn", "https://www.codecademy.com/es/Israelmercadopatio", "https://www.codecademy.com/SuperNovaP", "https://www.codecademy.com/Garchibulin", "https://www.codecademy.com/es/CarmenMorales"]
    end
end

